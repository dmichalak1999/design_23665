﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameNoughtsAndCrosses
{
    public partial class DeletePlayer : Form
    {
        public DeletePlayer()
        {
            InitializeComponent();
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnRemove_Click(object sender, EventArgs e)
        {
            SqlLiteDataAccess.RemovePerson(txbDelete.Text);
            this.Close();
        }
    }
}
