﻿namespace GameNoughtsAndCrosses
{
    partial class GameRun
    {
        /// <summary>
        /// Wymagana zmienna projektanta.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Wyczyść wszystkie używane zasoby.
        /// </summary>
        /// <param name="disposing">prawda, jeżeli zarządzane zasoby powinny zostać zlikwidowane; Fałsz w przeciwnym wypadku.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Kod generowany przez Projektanta formularzy systemu Windows

        /// <summary>
        /// Metoda wymagana do obsługi projektanta — nie należy modyfikować
        /// jej zawartości w edytorze kodu.
        /// </summary>
        private void InitializeComponent()
        {
            this.list = new System.Windows.Forms.DataGridView();
            this.btn1 = new System.Windows.Forms.Button();
            this.btn2 = new System.Windows.Forms.Button();
            this.btn3 = new System.Windows.Forms.Button();
            this.btn4 = new System.Windows.Forms.Button();
            this.btn5 = new System.Windows.Forms.Button();
            this.btn6 = new System.Windows.Forms.Button();
            this.btn7 = new System.Windows.Forms.Button();
            this.btn8 = new System.Windows.Forms.Button();
            this.btn9 = new System.Windows.Forms.Button();
            this.btnNew = new System.Windows.Forms.Button();
            this.btnPlay = new System.Windows.Forms.Button();
            this.btnStop = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.labelPlayerOne = new System.Windows.Forms.Label();
            this.labelPlayerTwo = new System.Windows.Forms.Label();
            this.txbOne = new System.Windows.Forms.TextBox();
            this.txbTwo = new System.Windows.Forms.TextBox();
            this.labelWinOne = new System.Windows.Forms.Label();
            this.labelWinTwo = new System.Windows.Forms.Label();
            this.lbWinOne = new System.Windows.Forms.Label();
            this.lbWinTwo = new System.Windows.Forms.Label();
            this.btnDelete = new System.Windows.Forms.Button();
            this.btnRefresh = new System.Windows.Forms.Button();
            this.lb = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.list)).BeginInit();
            this.SuspendLayout();
            // 
            // list
            // 
            this.list.AllowUserToAddRows = false;
            this.list.AllowUserToDeleteRows = false;
            this.list.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.list.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.list.Location = new System.Drawing.Point(490, 96);
            this.list.Name = "list";
            this.list.ReadOnly = true;
            this.list.Size = new System.Drawing.Size(316, 323);
            this.list.TabIndex = 0;
            // 
            // btn1
            // 
            this.btn1.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn1.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btn1.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F);
            this.btn1.Location = new System.Drawing.Point(41, 181);
            this.btn1.Name = "btn1";
            this.btn1.Size = new System.Drawing.Size(75, 75);
            this.btn1.TabIndex = 1;
            this.btn1.UseVisualStyleBackColor = false;
            this.btn1.Visible = false;
            this.btn1.Click += new System.EventHandler(this.btn1_Click);
            // 
            // btn2
            // 
            this.btn2.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn2.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btn2.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F);
            this.btn2.Location = new System.Drawing.Point(122, 181);
            this.btn2.Name = "btn2";
            this.btn2.Size = new System.Drawing.Size(75, 75);
            this.btn2.TabIndex = 2;
            this.btn2.UseVisualStyleBackColor = false;
            this.btn2.Visible = false;
            this.btn2.Click += new System.EventHandler(this.btn2_Click);
            // 
            // btn3
            // 
            this.btn3.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn3.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btn3.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F);
            this.btn3.Location = new System.Drawing.Point(203, 181);
            this.btn3.Name = "btn3";
            this.btn3.Size = new System.Drawing.Size(75, 75);
            this.btn3.TabIndex = 3;
            this.btn3.UseVisualStyleBackColor = false;
            this.btn3.Visible = false;
            this.btn3.Click += new System.EventHandler(this.btn3_Click);
            // 
            // btn4
            // 
            this.btn4.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn4.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btn4.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F);
            this.btn4.Location = new System.Drawing.Point(41, 262);
            this.btn4.Name = "btn4";
            this.btn4.Size = new System.Drawing.Size(75, 75);
            this.btn4.TabIndex = 4;
            this.btn4.UseVisualStyleBackColor = false;
            this.btn4.Visible = false;
            this.btn4.Click += new System.EventHandler(this.btn4_Click);
            // 
            // btn5
            // 
            this.btn5.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn5.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btn5.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F);
            this.btn5.Location = new System.Drawing.Point(122, 262);
            this.btn5.Name = "btn5";
            this.btn5.Size = new System.Drawing.Size(75, 75);
            this.btn5.TabIndex = 5;
            this.btn5.UseVisualStyleBackColor = false;
            this.btn5.Visible = false;
            this.btn5.Click += new System.EventHandler(this.btn5_Click);
            // 
            // btn6
            // 
            this.btn6.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn6.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btn6.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F);
            this.btn6.Location = new System.Drawing.Point(203, 262);
            this.btn6.Name = "btn6";
            this.btn6.Size = new System.Drawing.Size(75, 75);
            this.btn6.TabIndex = 6;
            this.btn6.UseVisualStyleBackColor = false;
            this.btn6.Visible = false;
            this.btn6.Click += new System.EventHandler(this.btn6_Click);
            // 
            // btn7
            // 
            this.btn7.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn7.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btn7.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F);
            this.btn7.Location = new System.Drawing.Point(41, 343);
            this.btn7.Name = "btn7";
            this.btn7.Size = new System.Drawing.Size(75, 75);
            this.btn7.TabIndex = 7;
            this.btn7.UseVisualStyleBackColor = false;
            this.btn7.Visible = false;
            this.btn7.Click += new System.EventHandler(this.btn7_Click);
            // 
            // btn8
            // 
            this.btn8.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn8.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btn8.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F);
            this.btn8.Location = new System.Drawing.Point(122, 343);
            this.btn8.Name = "btn8";
            this.btn8.Size = new System.Drawing.Size(75, 75);
            this.btn8.TabIndex = 8;
            this.btn8.UseVisualStyleBackColor = false;
            this.btn8.Visible = false;
            this.btn8.Click += new System.EventHandler(this.btn8_Click);
            // 
            // btn9
            // 
            this.btn9.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btn9.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btn9.Font = new System.Drawing.Font("Microsoft Sans Serif", 32F);
            this.btn9.Location = new System.Drawing.Point(203, 343);
            this.btn9.Name = "btn9";
            this.btn9.Size = new System.Drawing.Size(75, 75);
            this.btn9.TabIndex = 9;
            this.btn9.UseVisualStyleBackColor = false;
            this.btn9.Visible = false;
            this.btn9.Click += new System.EventHandler(this.btn9_Click);
            // 
            // btnNew
            // 
            this.btnNew.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnNew.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnNew.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnNew.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnNew.Location = new System.Drawing.Point(350, 181);
            this.btnNew.Name = "btnNew";
            this.btnNew.Size = new System.Drawing.Size(75, 75);
            this.btnNew.TabIndex = 10;
            this.btnNew.Text = "New User";
            this.btnNew.UseVisualStyleBackColor = false;
            this.btnNew.Click += new System.EventHandler(this.btnNew_Click);
            // 
            // btnPlay
            // 
            this.btnPlay.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnPlay.BackColor = System.Drawing.Color.Lime;
            this.btnPlay.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnPlay.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnPlay.Location = new System.Drawing.Point(350, 262);
            this.btnPlay.Name = "btnPlay";
            this.btnPlay.Size = new System.Drawing.Size(75, 75);
            this.btnPlay.TabIndex = 11;
            this.btnPlay.Text = "Play";
            this.btnPlay.UseVisualStyleBackColor = false;
            this.btnPlay.Click += new System.EventHandler(this.btnPlay_Click);
            // 
            // btnStop
            // 
            this.btnStop.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnStop.BackColor = System.Drawing.Color.Tomato;
            this.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnStop.Font = new System.Drawing.Font("Segoe UI Semibold", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.btnStop.Location = new System.Drawing.Point(350, 343);
            this.btnStop.Name = "btnStop";
            this.btnStop.Size = new System.Drawing.Size(75, 75);
            this.btnStop.TabIndex = 12;
            this.btnStop.Text = "Stop";
            this.btnStop.UseVisualStyleBackColor = false;
            this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.Orange;
            this.panel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.panel1.Location = new System.Drawing.Point(0, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(898, 78);
            this.panel1.TabIndex = 13;
            // 
            // labelPlayerOne
            // 
            this.labelPlayerOne.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelPlayerOne.AutoSize = true;
            this.labelPlayerOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelPlayerOne.Location = new System.Drawing.Point(37, 102);
            this.labelPlayerOne.Name = "labelPlayerOne";
            this.labelPlayerOne.Size = new System.Drawing.Size(65, 20);
            this.labelPlayerOne.TabIndex = 0;
            this.labelPlayerOne.Text = "Player 1";
            // 
            // labelPlayerTwo
            // 
            this.labelPlayerTwo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelPlayerTwo.AutoSize = true;
            this.labelPlayerTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelPlayerTwo.Location = new System.Drawing.Point(37, 138);
            this.labelPlayerTwo.Name = "labelPlayerTwo";
            this.labelPlayerTwo.Size = new System.Drawing.Size(65, 20);
            this.labelPlayerTwo.TabIndex = 15;
            this.labelPlayerTwo.Text = "Player 2";
            // 
            // txbOne
            // 
            this.txbOne.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txbOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txbOne.Location = new System.Drawing.Point(108, 96);
            this.txbOne.Name = "txbOne";
            this.txbOne.Size = new System.Drawing.Size(89, 26);
            this.txbOne.TabIndex = 16;
            // 
            // txbTwo
            // 
            this.txbTwo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txbTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.txbTwo.Location = new System.Drawing.Point(108, 135);
            this.txbTwo.Name = "txbTwo";
            this.txbTwo.Size = new System.Drawing.Size(89, 26);
            this.txbTwo.TabIndex = 17;
            // 
            // labelWinOne
            // 
            this.labelWinOne.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelWinOne.AutoSize = true;
            this.labelWinOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelWinOne.Location = new System.Drawing.Point(203, 102);
            this.labelWinOne.Name = "labelWinOne";
            this.labelWinOne.Size = new System.Drawing.Size(40, 20);
            this.labelWinOne.TabIndex = 18;
            this.labelWinOne.Text = "Win:";
            // 
            // labelWinTwo
            // 
            this.labelWinTwo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.labelWinTwo.AutoSize = true;
            this.labelWinTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.labelWinTwo.Location = new System.Drawing.Point(203, 138);
            this.labelWinTwo.Name = "labelWinTwo";
            this.labelWinTwo.Size = new System.Drawing.Size(40, 20);
            this.labelWinTwo.TabIndex = 19;
            this.labelWinTwo.Text = "Win:";
            // 
            // lbWinOne
            // 
            this.lbWinOne.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbWinOne.AutoSize = true;
            this.lbWinOne.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lbWinOne.Location = new System.Drawing.Point(243, 102);
            this.lbWinOne.Name = "lbWinOne";
            this.lbWinOne.Size = new System.Drawing.Size(0, 20);
            this.lbWinOne.TabIndex = 20;
            // 
            // lbWinTwo
            // 
            this.lbWinTwo.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lbWinTwo.AutoSize = true;
            this.lbWinTwo.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.lbWinTwo.Location = new System.Drawing.Point(243, 138);
            this.lbWinTwo.Name = "lbWinTwo";
            this.lbWinTwo.Size = new System.Drawing.Size(0, 20);
            this.lbWinTwo.TabIndex = 21;
            // 
            // btnDelete
            // 
            this.btnDelete.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnDelete.BackColor = System.Drawing.Color.Tomato;
            this.btnDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDelete.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnDelete.Location = new System.Drawing.Point(812, 96);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(75, 75);
            this.btnDelete.TabIndex = 22;
            this.btnDelete.Text = "Delete Player";
            this.btnDelete.UseVisualStyleBackColor = false;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.btnRefresh.BackColor = System.Drawing.Color.DodgerBlue;
            this.btnRefresh.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnRefresh.Font = new System.Drawing.Font("Segoe UI Semibold", 12F, System.Drawing.FontStyle.Bold);
            this.btnRefresh.Location = new System.Drawing.Point(812, 177);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(76, 75);
            this.btnRefresh.TabIndex = 23;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.UseVisualStyleBackColor = false;
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // lb
            // 
            this.lb.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lb.AutoSize = true;
            this.lb.BackColor = System.Drawing.Color.Orange;
            this.lb.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.lb.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F);
            this.lb.Location = new System.Drawing.Point(3, 23);
            this.lb.Name = "lb";
            this.lb.Size = new System.Drawing.Size(800, 55);
            this.lb.TabIndex = 14;
            this.lb.Text = "Podaj Nazwy graczy i naciśnij Play   ";
            // 
            // GameRun
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightSeaGreen;
            this.ClientSize = new System.Drawing.Size(898, 450);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.lbWinTwo);
            this.Controls.Add(this.lbWinOne);
            this.Controls.Add(this.labelWinTwo);
            this.Controls.Add(this.labelWinOne);
            this.Controls.Add(this.txbTwo);
            this.Controls.Add(this.txbOne);
            this.Controls.Add(this.labelPlayerTwo);
            this.Controls.Add(this.labelPlayerOne);
            this.Controls.Add(this.lb);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.btnStop);
            this.Controls.Add(this.btnPlay);
            this.Controls.Add(this.btnNew);
            this.Controls.Add(this.btn9);
            this.Controls.Add(this.btn8);
            this.Controls.Add(this.btn7);
            this.Controls.Add(this.btn6);
            this.Controls.Add(this.btn5);
            this.Controls.Add(this.btn4);
            this.Controls.Add(this.btn3);
            this.Controls.Add(this.btn2);
            this.Controls.Add(this.btn1);
            this.Controls.Add(this.list);
            this.Name = "GameRun";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "NoughtsAndCrosses";
            this.Load += new System.EventHandler(this.lb_Load);
            ((System.ComponentModel.ISupportInitialize)(this.list)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView list;
        private System.Windows.Forms.Button btn1;
        private System.Windows.Forms.Button btn2;
        private System.Windows.Forms.Button btn3;
        private System.Windows.Forms.Button btn4;
        private System.Windows.Forms.Button btn5;
        private System.Windows.Forms.Button btn6;
        private System.Windows.Forms.Button btn7;
        private System.Windows.Forms.Button btn8;
        private System.Windows.Forms.Button btn9;
        private System.Windows.Forms.Button btnNew;
        private System.Windows.Forms.Button btnPlay;
        private System.Windows.Forms.Button btnStop;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label labelPlayerOne;
        private System.Windows.Forms.Label labelPlayerTwo;
        private System.Windows.Forms.TextBox txbOne;
        private System.Windows.Forms.TextBox txbTwo;
        private System.Windows.Forms.Label labelWinOne;
        private System.Windows.Forms.Label labelWinTwo;
        private System.Windows.Forms.Label lbWinOne;
        private System.Windows.Forms.Label lbWinTwo;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Button btnRefresh;
        private System.Windows.Forms.Label lb;
    }
}

