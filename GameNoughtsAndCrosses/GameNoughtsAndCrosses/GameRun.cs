﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace GameNoughtsAndCrosses
{
    public partial class GameRun : Form
    {
        List<Player> players = new List<Player>();
        private int start = 0;
        private string playerOne;
        private string playerTwo;

        public string PlayerOne { get; set; }
        public string PlayerTwo { get; set; }
        public int Start { get { return start; } set { start = value; } }
      
        public GameRun()
        {
            InitializeComponent();
            LoadPeopleList();
        }
        public void LoadPeopleList()
        {
            players = SqlLiteDataAccess.LoadPeople();
            WireUPPeopleList();
            foreach (var item in players)
            {
                if (item.Name == txbOne.Text)
                    lbWinOne.Text = item.Win.ToString();
                if (item.Name == txbTwo.Text)
                    lbWinTwo.Text = item.Win.ToString();
            }
        }
        private void WireUPPeopleList()
        {
            list.DataSource = null;
            list.DataSource = players;
        }
        private void addPlayer(string playerOne, string playerTwo)
        {

            Player p = new Player();
            p.Name = playerOne;
            SqlLiteDataAccess.SavePerson(p);
            p.Name = playerTwo;
            SqlLiteDataAccess.SavePerson(p);
            WireUPPeopleList();
            LoadPeopleList();

        }
        private void addWin(string winner)
        {
            SqlLiteDataAccess.EditPerson(winner);
            LoadPeopleList();
        }
        public string CheckPlayers()
        {
            if (Start == 0)
                Start = RandomPlayers();
            Start++;
            Convert.ToInt32(start);
            if (Start % 2 == 0)
            {
                lb.Text = $"Ruch Gracza X ({PlayerOne})";
                return "O";
            }
            else
            {
                lb.Text = $"Ruch Gracza O ({PlayerTwo})";
                return "X";
            }
        }
        public void Winner(string btn)
        {
            if (btn == "X")
            {
                lb.Text = $"Wygrał gracz {btn} ({PlayerOne}) ";
                MessageBox.Show($"Wygrał gracz {btn} ({PlayerOne})");
                addWin(PlayerOne);
                lb.Text = "Naciśnij Play ,żeby zagrać";
                btn1.Enabled = false; btn2.Enabled = false; btn3.Enabled = false; btn4.Enabled = false; btn5.Enabled = false; btn6.Enabled = false;
                btn7.Enabled = false; btn8.Enabled = false; btn9.Enabled = false;
            }
            if (btn == "O")
            {
                lb.Text = $"Wygrał gracz {btn} ({PlayerTwo}) ";
                MessageBox.Show($"Wygrał gracz {btn} ({PlayerTwo}) ");
                addWin(PlayerTwo);
                lb.Text = "Naciśnij Play ,żeby zagrać";
                btn1.Enabled = false; btn2.Enabled = false; btn3.Enabled = false; btn4.Enabled = false; btn5.Enabled = false; btn6.Enabled = false;
                btn7.Enabled = false; btn8.Enabled = false; btn9.Enabled = false;
            }
        }
        public void WinCheck()
        {

            if (btn1.Text == btn2.Text && btn1.Text == btn3.Text && btn1.Text != "")
                Winner(btn1.Text);
            else if (btn4.Text == btn5.Text && btn4.Text == btn6.Text && btn4.Text != "")
                Winner(btn4.Text);
            else if (btn7.Text == btn8.Text && btn7.Text == btn9.Text && btn7.Text != "")
                Winner(btn7.Text);
            else if (btn1.Text == btn4.Text && btn1.Text == btn7.Text && btn1.Text != "")
                Winner(btn1.Text);
            else if (btn2.Text == btn5.Text && btn2.Text == btn8.Text && btn2.Text != "")
                Winner(btn2.Text);
            else if (btn3.Text == btn6.Text && btn3.Text == btn9.Text && btn3.Text != "")
                Winner(btn3.Text);
            else if (btn1.Text == btn5.Text && btn1.Text == btn9.Text && btn1.Text != "")
                Winner(btn1.Text);
            else if (btn3.Text == btn5.Text && btn3.Text == btn7.Text && btn7.Text != "")
                Winner(btn3.Text);
            else if (btn1.Text != "" && btn2.Text != "" && btn3.Text != "" && btn4.Text != "" && btn5.Text != "" && btn6.Text != "" && btn7.Text != "" && btn8.Text != "" && btn9.Text != "")
            {
                MessageBox.Show("Nikt nie wygrał!");
                lb.Text = "Naciśnij Play ,żeby zagrać";
            }
        }
        public void RemoveButton()
        {
            btn1.Text = "";
            btn2.Text = "";
            btn3.Text = "";
            btn4.Text = "";
            btn5.Text = "";
            btn6.Text = "";
            btn7.Text = "";
            btn8.Text = "";
            btn9.Text = "";
        }
        public void Stop()
        {
            btn1.Visible = false;
            btn2.Visible = false;
            btn3.Visible = false;
            btn4.Visible = false;
            btn5.Visible = false;
            btn6.Visible = false;
            btn7.Visible = false;
            btn8.Visible = false;
            btn9.Visible = false;
            lb.Text = "Naciśnij Play ,żeby zagrać ";
        }
        public int RandomPlayers()
        {
            Random random = new Random();
            int start = random.Next(0, 2);
            return start;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }
        private void btnPlay_Click(object sender, EventArgs e)
        {
            if (txbOne.Text != "" && txbTwo.Text != "")
            {
                PlayerOne = txbOne.Text;
                PlayerTwo = txbTwo.Text;
                CheckPlayers();
                RemoveButton();
                btn1.Visible = true;
                btn2.Visible = true;
                btn3.Visible = true;
                btn4.Visible = true;
                btn5.Visible = true;
                btn6.Visible = true;
                btn7.Visible = true;
                btn8.Visible = true;
                btn9.Visible = true;
                addPlayer(PlayerOne, PlayerTwo);
                btn1.Enabled = true; btn2.Enabled = true; btn3.Enabled = true; btn4.Enabled = true; btn5.Enabled = true; btn6.Enabled = true;
                btn7.Enabled = true; btn8.Enabled = true; btn9.Enabled = true;
            }
            else
            {
                lb.Text = "Podaj Nazwy graczy i naciśnij Play";
            }
        }
        private void btnStop_Click(object sender, EventArgs e)
        {
            RemoveButton();
            Stop();
        }
        private void btn1_Click(object sender, EventArgs e)
        {
            if (btn1.Text == "")
            {
                btn1.Text = CheckPlayers();
                WinCheck();
            }
        }

        private void btn2_Click(object sender, EventArgs e)
        {
            if (btn2.Text == "")
            {
                btn2.Text = CheckPlayers();
                WinCheck();
            }
        }

        private void btn3_Click(object sender, EventArgs e)
        {
            if (btn3.Text == "")
            {
                btn3.Text = CheckPlayers();
                WinCheck();
            }
        }

        private void btn4_Click(object sender, EventArgs e)
        {
            if (btn4.Text == "")
            {
                btn4.Text = CheckPlayers();
                WinCheck();
            }
        }

        private void btn5_Click(object sender, EventArgs e)
        {
            if (btn5.Text == "")
            {
                btn5.Text = CheckPlayers();
                WinCheck();
            }
        }

        private void btn6_Click(object sender, EventArgs e)
        {
            if (btn6.Text == "")
            {
                btn6.Text = CheckPlayers();
                WinCheck();
            }
        }
        private void btn7_Click(object sender, EventArgs e)
        {
            if (btn7.Text == "")
            {
                btn7.Text = CheckPlayers();
                WinCheck();
            }
        }
        private void btn8_Click(object sender, EventArgs e)
        {
            if (btn8.Text == "")
            {
                btn8.Text = CheckPlayers();
                WinCheck();
            }
        }

        private void btn9_Click(object sender, EventArgs e)
        {
            if (btn9.Text == "")
            {
                btn9.Text = CheckPlayers();
                WinCheck();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeletePlayer deleteplayer = new DeletePlayer();
            deleteplayer.ShowDialog();

        }

        private void btnRefresh_Click(object sender, EventArgs e)
        {
            LoadPeopleList();
        }

        private void btnNew_Click(object sender, EventArgs e)
        {
            txbOne.Text = "";
            txbTwo.Text = "";
            lbWinOne.Text = "";
            lbWinTwo.Text = "";
            Stop();
            lb.Text = "Podaj Nazwy graczy i naciśnij Play ";

        }

        private void lb_Load(object sender, EventArgs e)
        {
        }
    }
}
