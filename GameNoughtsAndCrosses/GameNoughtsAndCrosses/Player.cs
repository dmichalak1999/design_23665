﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameNoughtsAndCrosses
{
    class Player
    {
        private int id;
        private int win;
        private string name;
        public int Id { get; set; }
        public string Name { get; set; }
        public int Win { get; set; }
    }
}
